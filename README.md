# About Netlify git-gateway builder
This project builds and publishes the Docker image of our forked version of Netlify git-gateway.

Note: The server requires a handful of env variables.  See [example.env](https://github.com/netlify/git-gateway/blob/master/example.env).

```
docker run -e <VARIABLE_NAME=VALUE> registry.gitlab.com/openbeta/netlify-git-gateway:latest 

```
